#!/bin/bash

OUTDIR=dist

mkdir -p $OUTDIR

# Apple
convert -background none -filter Lanczos -distort resize 180x logo-fill-color.svg $OUTDIR/asciidoc-logo-apple-touch-180.png
# Microsoft
convert -background none -filter Lanczos -distort resize 70x logo-fill-white.svg $OUTDIR/asciidoc-logo-mstile-70.png
convert -background none -filter Lanczos -distort resize 150x logo-fill-white.svg $OUTDIR/asciidoc-logo-mstile-150.png
convert -background none -gravity center -extent 310x150 $OUTDIR/asciidoc-logo-mstile-150.png $OUTDIR/asciidoc-logo-mstile-310x150.png
convert -background none -filter Lanczos -distort resize 310x logo-fill-white.svg $OUTDIR/asciidoc-logo-mstile-310.png
# Standard
convert -background none -filter Lanczos -distort resize 512x logo-fill-color.svg $OUTDIR/asciidoc-logo-512.png
convert -background none -filter Lanczos -distort resize 196x logo-fill-color.svg $OUTDIR/asciidoc-logo-196.png
convert -background none -filter Lanczos -distort resize 192x logo-fill-color.svg $OUTDIR/asciidoc-logo-192.png
convert -background none -filter Lanczos -distort resize 128x logo-fill-color.svg $OUTDIR/asciidoc-logo-128.png
convert -background none -filter Lanczos -distort resize 96x logo-fill-color.svg $OUTDIR/asciidoc-logo-96.png
convert -background none -filter Lanczos -distort resize 64x logo-fill-color.svg $OUTDIR/asciidoc-logo-64.png
convert -background none -filter Lanczos -distort resize 48x logo-fill-color.svg $OUTDIR/asciidoc-logo-48.png
convert -background none -filter Lanczos -distort resize 32x logo-fill-color.svg $OUTDIR/asciidoc-logo-32.png
convert -background none -filter Lanczos -distort resize 16x -unsharp 0x1+.5+.25 logo-fill-color.svg $OUTDIR/asciidoc-logo-16.png

icotool -c -o $OUTDIR/favicon.ico \
  $OUTDIR/asciidoc-logo-64.png \
  $OUTDIR/asciidoc-logo-48.png \
  $OUTDIR/asciidoc-logo-32.png \
  $OUTDIR/asciidoc-logo-16.png
